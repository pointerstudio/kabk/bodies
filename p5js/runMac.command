i#!/bin/bash
currentDirectory="$(dirname "$BASH_SOURCE")"
cd "$currentDirectory"
npm install -g browser-sync
browser-sync start --server --https --files ["* .html", "* .css", "* .js"]
