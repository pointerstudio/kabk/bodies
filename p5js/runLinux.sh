#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

echo "make sure you install browser-sync"

browser-sync start --server --watch --https --files ["* .html", "* .css", "* .js"]
