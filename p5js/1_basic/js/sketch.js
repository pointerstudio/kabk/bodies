/* KABK * IMD * skeleton tracking example
 *
 * basic
 *
 * In this example, you'll see how to track if someone is in front of the camera or not.
 *
 */

// GLOBAL VARIABLES
// variables, that will be accessible anywhere on this website,
// even in other scripts (if they are loaded after this script)

// one for the videocamera
let video;

// one for poseNet, this is the skeleton (pose) detector
let poseNet;

// one array for all poses
// an array is a list
let poses = [];

// the video size, smaller means faster
// first the width
// feel free to set this to 320 if it is otherwise tough for your computer
let videoWidth = 640;
// then the height
// feel free to set this to 240 if it is otherwise tough for your computer
let videoHeight = 480;


// the setup function runs exactly once at the beginning
// we can use it to... set up things
function setup() {
    // first, create a canvas to draw into
    // p5js uses a canvas for drawing
    createCanvas(windowWidth, windowHeight);

    // this is how p5js is using your webcam
    video = createCapture(VIDEO);
    // then give it the size that you want
    video.size(videoWidth, videoHeight);

    // Create a new poseNet method
    // call the modelReady function, when ready
    poseNet = ml5.poseNet(video);

    // This sets up an event that fills the global variable "poses"
    // with an array every time new poses are detected
    poseNet.on('pose', function(results) {
        poses = results;
    });

    // Hide the video element, and just show the canvas
    video.hide();
}

// is is called over and over and over again
function draw() {
    // first, let's draw a black background
    background(0);

    // and if anyone is there..
    if (poses.length > 0) {
        // let's be polite and say hello

        // first, text color
        // check https://p5js.org/reference/#/p5/fill
        fill('red');
        // then set text size
        // check https://p5js.org/reference/#/p5/textSize
        textSize(32);
        // and finally the text
        // check https://p5js.org/reference/#/p5/text
        text("hello",100,100);
    }

}
