/* KABK * IMD * skeleton tracking example
 *
 * webcam dots
 *
 * In this example, you'll see how to track skeletons from a webcam,
 * and draw dots where the joints would be.
 *
 */

// GLOBAL VARIABLES
// variables, that will be accessible anywhere on this website,
// even in other scripts (if they are loaded after this script)

// one for the videocamera
let video;

// one for poseNet, this is the skeleton (pose) detector
let poseNet;

// one array for all poses
// an array is a list
let poses = [];

// the video size, smaller means faster
// first the width
// feel free to set this to 320 if it is otherwise tough for your computer
let videoWidth = 640;
// then the height
// feel free to set this to 240 if it is otherwise tough for your computer
let videoHeight = 480;


// the setup function runs exactly once at the beginning
// we can use it to... set up things
function setup() {
    // first, create a canvas to draw into
    // p5js uses a canvas for drawing
    createCanvas(windowWidth, windowHeight);

    // this is how p5js is using your webcam
    video = createCapture(VIDEO);
    // then give it the size that you want
    video.size(videoWidth, videoHeight);

    // Create a new poseNet method
    // call the modelReady function, when ready
    poseNet = ml5.poseNet(video, modelReady);

    // This sets up an event that fills the global variable "poses"
    // with an array every time new poses are detected
    poseNet.on('pose', function(results) {
        poses = results;
    });

    // Hide the video element, and just show the canvas
    video.hide();
}

// when posenet is ready it calls this function
function modelReady() {
    select('#status').html('Model Loaded');
}


function draw() {
    // first, let's draw a black background
  background(0);

    // then the video, just so that we see what it's doing
  image(video,0,0,videoWidth,videoHeight);

  // let's have different colors for different people (a.k.a. poses)
  let colors = ['red','green','blue','pink','purple'];

  // Loop through all the poses detected
  for (let i = 0; i < poses.length; i++) {
    // For each pose detected, loop through all the keypoints
    let pose = poses[i].pose;

    for (let j = 0; j < pose.keypoints.length; j++) {
      // A keypoint is an object describing a body part (like rightArm or leftShoulder)
      let keypoint = pose.keypoints[j];
      // Only draw an ellipse is the pose probability is bigger than 0.2
      if (keypoint.score > 0.2) {
        fill(colors[i]);
        noStroke();
        ellipse(keypoint.position.x, keypoint.position.y, 10, 10);
      }
    }
  }
}
