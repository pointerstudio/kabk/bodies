/* KABK * IMD * skeleton tracking example
 *
 * webcam dots
 *
 * In this example, you'll see how to track skeletons from a webcam,
 * and draw dots where the joints would be.
 *
 */

// GLOBAL VARIABLES
// variables, that will be accessible anywhere on this website,
// even in other scripts (if they are loaded after this script)

// one for the videocamera
let video;

// one for poseNet, this is the skeleton (pose) detector
let poseNet;

// one array for all poses
// an array is a list
let poses = [];

// the video size, smaller means faster
// first the width
// feel free to set this to 320 if it is otherwise tough for your computer
let videoWidth = 640;
// then the height
// feel free to set this to 240 if it is otherwise tough for your computer
let videoHeight = 480;

// the recording will just be text
// so let's start with an empty string
let recording = '';

// why not have a handy checkbox
let recordingCheckbox;

// and a save button
let recordingSaveButton;

// the setup function runs exactly once at the beginning
// we can use it to... set up things
function setup() {
    // first, create a canvas to draw into
    // p5js uses a canvas for drawing
    createCanvas(windowWidth, windowHeight);

    // this is how p5js is using your webcam
    video = createCapture(VIDEO);
    // then give it the size that you want
    video.size(videoWidth, videoHeight);

    // Create a new poseNet method
    // call the modelReady function, when ready
    poseNet = ml5.poseNet(video, modelReady);

    // This sets up an event that fills the global variable "poses"
    // with an array every time new poses are detected
    poseNet.on('pose', function(results) {
      // only return poses that are above a 0.2 score
      poses = results.filter((e) => {return e.pose.score > 0.2});

      // check if we are recording
      if (recordingCheckbox.checked()) {
        // wrap everything so that unity also knows the dimensions of the camera
        let poseNetData = {
            "image": {
                "width": videoWidth,
                "height": videoHeight
            },
            "poses": poses

        };
        // and add the whole frame as a string to our recording
        recording += JSON.stringify(poseNetData);
        // separate recording frames with a semicolon
        // because who doesn't love semicolons
        recording += ";";
      }
    });

    // Hide the video element, and just show the canvas
    video.hide();

    // how to set up a checkbox
    // https://p5js.org/reference/#/p5/createcheckbox
    recordingCheckbox = createCheckbox('record', false);
    recordingCheckbox.position(0, 0);

    // and similarly with a button
    // https://p5js.org/reference/#/p5/createbutton
    recordingSaveButton = createButton('save recording');
    recordingSaveButton.position(4, 20);
    // most importantly, what happens when the button is pressed
    // in our case, save the recording as a textfile
    recordingSaveButton.mousePressed(function() {
      saveText(recording);
    });
}

// when posenet is ready it calls this function
function modelReady() {
    select('#status').html('Model Loaded');
}


function draw() {
    // first, let's draw a white background
  background(255);

    // then the video, just so that we see what it's doing
  image(video,0,0,videoWidth,videoHeight);

  // let's have different colors for different people (a.k.a. poses)
  let colors = ['pink','blue','green','purple'];

  // Loop through all the poses detected
  for (let i = 0; i < poses.length; i++) {
    // For each pose detected, loop through all the keypoints
    let pose = poses[i].pose;
    let skeleton = poses[i].skeleton;

    for (let j = 0; j < pose.keypoints.length; j++) {
      // A keypoint is an object describing a body part (like rightArm or leftShoulder)
      let keypoint = pose.keypoints[j];
      // Only draw an ellipse is the pose probability is bigger than 0.2
      if (keypoint.score > 0.2) {
        noStroke();
        if (recordingCheckbox.checked()) {
          fill('red');
          ellipse(keypoint.position.x, keypoint.position.y, 15 + Math.sin(millis() * 0.01) * 5, 15 + Math.sin(millis() * 0.01) * 5);
        }
        fill(colors[i]);
        ellipse(keypoint.position.x, keypoint.position.y, 10, 10);
      }
    }

    for (let j = 0; j < skeleton.length; j++) {
      if (recordingCheckbox.checked()) {
        stroke('red');
        strokeWeight(7 + Math.sin(millis() * 0.01) * 5);
        line(skeleton[j][0].position.x,
             skeleton[j][0].position.y,
             skeleton[j][1].position.x,
             skeleton[j][1].position.y);
      }
      stroke(colors[i]);
      strokeWeight(2);
      line(skeleton[j][0].position.x,
           skeleton[j][0].position.y,
           skeleton[j][1].position.x,
           skeleton[j][1].position.y);
    }
  }

  fill(255);
  rect(0,0,120,46);
}

// you could easily also just use save(variable,fileName)
// https://p5js.org/reference/#/p5/save
// but for some historically relevant reasons I used this
// and it works, so I kept it
function saveText(textToSave) {
  var hiddenElement = document.createElement('a');

  hiddenElement.href = 'data:attachment/text,' + encodeURI(textToSave);
  hiddenElement.target = '_blank';
  hiddenElement.download = "recording-"+new Date().toISOString()+".txt";
  hiddenElement.click();
}
