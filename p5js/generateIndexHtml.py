#!/usr/bin/env python3

import os


def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

html = """<!DOCTYPE html>
<html>
<head>
<title>p5js posenet examples</title>
</head>
<body>
"""


for top, dirs, files in walklevel(os.getcwd(),0):
    print("Printing directories...")
    dirs.sort()
    for dir in dirs:
        html += "    <a href='./{d}/'>{d}</a><br>\n".format(d=dir)
        print(dir)

html += """</body>
</html>"""

textfile = open("index.html", "w")
a = textfile.write(html)
textfile.close()
