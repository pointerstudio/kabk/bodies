/* KABK * IMD * skeleton tracking example
 *
 * multibasic
 *
 * In this example, you'll see how to track if someone is in front of the camera or not.
 * Here, the more people there are, the more greetings there will be
 *
 */

// GLOBAL VARIABLES
// variables, that will be accessible anywhere on this website,
// even in other scripts (if they are loaded after this script)

// one for the videocamera
let video;

// one for poseNet, this is the skeleton (pose) detector
let poseNet;

// one array for all poses
// an array is a list
let poses = [];

// the video size, smaller means faster
// first the width
// feel free to set this to 320 if it is otherwise tough for your computer
let videoWidth = 640;
// then the height
// feel free to set this to 240 if it is otherwise tough for your computer
let videoHeight = 480;


// the setup function runs exactly once at the beginning
// we can use it to... set up things
function setup() {
    // first, create a canvas to draw into
    // p5js uses a canvas for drawing
    createCanvas(windowWidth, windowHeight);

    // this is how p5js is using your webcam
    video = createCapture(VIDEO);
    // then give it the size that you want
    video.size(videoWidth, videoHeight);

    // Create a new poseNet method
    // call the modelReady function, when ready
    poseNet = ml5.poseNet(video);

    // This sets up an event that fills the global variable "poses"
    // with an array every time new poses are detected
    poseNet.on('pose', function(results) {
        poses = results;
    });

    // Hide the video element, and just show the canvas
    video.hide();
}

// is is called over and over and over again
function draw() {
    // first, let's draw a black background
    background(0);

    // and go through each pose
    // the i value will increase per person
    // it will first be 0, then 1, then 2, then 3 and so on
    // check https://p5js.org/reference/#/p5/for
    for (let i = 0; i < poses.length; i++) {
        // first, text color
        // check https://p5js.org/reference/#/p5/fill
        fill('red');

        // then set text size
        // check https://p5js.org/reference/#/p5/textSize
        textSize(32);

        // x position of the text
        // this is just a random value
        let x = 32;
        // well actually it's not really random, this would be random:
        // x = random(32);
        // ... and actually this is also not really really random,
        // because it's just a pseudorandomly generated number between 0 and 32, and then... what is random anyway
        // check: https://en.wikipedia.org/wiki/Randomness
        // I need to stop getting distracted and write more of these examples for you
        // let's get back to it:

        // y position of the text
        // we want the greeting to be under each other
        // so, for each new person, it has to be shifted down
        // a good value for this shift would be the textSize from above
        // also, we need to add one to the index (i),
        // otherwise the first text would be above the canvas, not inside it
        // remember, i starts with 0
        let y = (i + 1) * 32;

        // and finally the text
        // check https://p5js.org/reference/#/p5/text
        text("hello", x, y);
    }
}
