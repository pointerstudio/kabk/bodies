/* KABK * IMD * skeleton tracking example
 *
 * distance
 *
 * In this example, you'll see how to track how close someone is to the camera.
 * The webcam itself does not give us depth information, so we actually do not know how close someone is.
 * What we do instead here is an approximation, where we simply measure the distance between the eyes.
 * logic: the closer someone is -> the larger the face on the image -> the larger the distance between eyes 
 * 
 */

// GLOBAL VARIABLES
// variables, that will be accessible anywhere on this website,
// even in other scripts (if they are loaded after this script)

// one for the videocamera
let video;

// one for poseNet, this is the skeleton (pose) detector
let poseNet;

// one array for all poses
// an array is a list
let poses = [];

// the video size, smaller means faster
// first the width
// feel free to set this to 320 if it is otherwise tough for your computer
let videoWidth = 640;
// then the height
// feel free to set this to 240 if it is otherwise tough for your computer
let videoHeight = 480;

// and here our distance
// start with setting it to false, so we know distance is not measured yet
let distance = false;

// define maximum and minimum distance
let minimumDistance = videoWidth/8;
let maximumDistance = 10;


// the setup function runs exactly once at the beginning
// we can use it to... set up things
function setup() {
    // first, create a canvas to draw into
    // p5js uses a canvas for drawing
    createCanvas(windowWidth, windowHeight);

    // this is how p5js is using your webcam
    video = createCapture(VIDEO);
    // then give it the size that you want
    video.size(videoWidth, videoHeight);

    // Create a new poseNet method
    // call the modelReady function, when ready
    poseNet = ml5.poseNet(video);

    // This sets up an event that fills the global variable "poses"
    // with an array every time new poses are detected
    poseNet.on('pose', function(results) {
        poses = results;
    });

    // Hide the video element, and just show the canvas
    video.hide();
}

// is is called over and over and over again
function draw() {
    // first, let's draw a black background
    background(0);

    // we will change textSize according to distance,
    // so let's keep track of it so our text lines don't get scrambled
    let textSizes = 0;

    // and go through each pose
    // the i value will increase per person
    // it will first be 0, then 1, then 2, then 3 and so on
    // check https://p5js.org/reference/#/p5/for
    for (let i = 0; i < poses.length; i++) {
        background('green');
        // first, text color
        // check https://p5js.org/reference/#/p5/fill
        fill('red');

        // calculate relative distance
        // we use abs() because a distance is always a positive value
        // check https://p5js.org/reference/#/p5/abs
        let x_distance = abs(poses[i].pose.leftEye.x - poses[i].pose.rightEye.x);
        let y_distance = abs(poses[i].pose.leftEye.y - poses[i].pose.rightEye.y);

        // whoops, math
        // https://www.youtube.com/watch?v=0IOEPcAHgi4
        let distance = sqrt(pow(x_distance,2) + pow(y_distance,2));

        // map the text Size to the distance
        let currentTextSize = map(distance,minimumDistance,maximumDistance,64,32,true);

        // add the textSize to our textSizes
        textSizes += currentTextSize;

        // then set text size
        // check https://p5js.org/reference/#/p5/textSize
        textSize(currentTextSize);

        // x position of the text
        // this is just a random value
        let x = 32;
        // well actually it's not really random, this would be random:
        // x = random(32);
        // ... and actually this is also not really really random,
        // because it's just a pseudorandomly generated number between 0 and 32, and then... what is random anyway
        // check: https://en.wikipedia.org/wiki/Randomness
        // I need to stop getting distracted and write more of these examples for you
        // let's get back to it:

        // y position of the text
        // we want the greeting to be under each other
        // so, for each new person, it has to be shifted down
        // a good value for this shift would be the textSize from above
        // remember, we added this to the allover textSizes from the lines before
        // so we can just use that
        let y = textSizes;

        // and finally the text
        // check https://p5js.org/reference/#/p5/text
        text("hello", x, y);
    }
}
