using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PoseMove : MonoBehaviour
{

    public Vector3 moveScale = new Vector3(10.0f,10.0f,10.0f);

    public void GetPoseNetPositions(List<Vector3> poseNetPositions)
    {
        // only change position if there is somebody
        if (poseNetPositions.Count > 0)
        {
            transform.localPosition = new Vector3(poseNetPositions[0].x * moveScale.x,
                                                  poseNetPositions[0].y * moveScale.y,
                                                  poseNetPositions[0].z * moveScale.z);
        }
        // otherwise put it in the middle
        else
        {
            transform.localPosition = new Vector3(0, 0, 0);
        }
    }
}
