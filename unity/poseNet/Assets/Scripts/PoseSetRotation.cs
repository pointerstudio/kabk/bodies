using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PoseSetRotation : MonoBehaviour
{

    public Vector2 rotationX = new Vector2(-180.0f,180.0f);
    public Vector2 rotationY = new Vector2(-180.0f,180.0f);
    public Vector2 rotationZ = new Vector2(-180.0f,180.0f);

    public void GetPoseNetPositions(List<Vector3> poseNetPositions)
    {
        // only change position if there is somebody
        if (poseNetPositions.Count > 0)
        {
            float x = MyMath.Mapped(poseNetPositions[0].x,
                                    -1, 1,
                                    rotationX.x, rotationX.y);
            float y = MyMath.Mapped(poseNetPositions[0].y,
                                    -1, 1,
                                    rotationY.x, rotationY.y);
            float z = MyMath.Mapped(poseNetPositions[0].z,
                                    -1, 1,
                                    rotationZ.x, rotationZ.y);

            transform.localRotation = Quaternion.Euler(x, y, z);
        }
    }
}
