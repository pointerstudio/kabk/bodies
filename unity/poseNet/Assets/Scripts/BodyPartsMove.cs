using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPartsMove : MonoBehaviour
{
    public Vector3 moveScale = new Vector3(10.0f,10.0f,10.0f);
    public SkeletonDefinitions bodyPart;
    public bool hideUndetected = true;

    Renderer renderer;

    void Start() {
        renderer = GetComponent<Renderer>();
        renderer.enabled = false;
    }

    public void GetPoseNetPoseSkeletons(List<UnityPoseSkeleton> poseSkeletons)
    {
        Debug.Log("receives poseSkeletons " + poseSkeletons.Count);
        // only change position if there is somebody
        if (poseSkeletons.Count > 0)
        {
            float score = poseSkeletons[0].pose.scores[(int)bodyPart];
            Vector3 keypoint = poseSkeletons[0].pose.keypoints[(int)bodyPart];
            if (score > 0.2) {
                renderer.enabled = true;
                transform.localPosition = new Vector3(keypoint.x * moveScale.x,
                                                      keypoint.y * moveScale.y,
                                                      keypoint.z * moveScale.z);
            } else {
                renderer.enabled = false;
            }
        }
        // otherwise disable
        else
        {
            renderer.enabled = false;
        }
    }
}
