using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/*************************************************************
 *          Definitions and Utilities
 */

// SkeletonDefinitions are just for convenience
public enum SkeletonDefinitions {
    Nose, LeftEye, RightEye, LeftEar, RightEar, LeftShoulder, RightShoulder, LeftElbow, RightElbow, LeftWrist, RightWrist, LeftHip, RightHip, LeftKnee, RightKnee, LeftAnkle, RightAnkle
};

// following classes are describing how the object looks like that we get as a json
// so we can directly load it in

[Serializable]
public class PoseNetPosition
{
    public float x;
    public float y;
}

[Serializable]
public class PoseNetKeyPoint
{
    public string part;
    public PoseNetPosition position;
    public float score;
}

[Serializable]
public class PoseNetBodyPart
{
    public float confidence;
    public float x;
    public float y;
}

[Serializable]
public class PoseNetPose
{
    public float score;
    public PoseNetKeyPoint[] keypoints;
    //public PoseNetBodyPart nose;
    //public PoseNetBodyPart leftEye;
    //public PoseNetBodyPart rightEye;
    //public PoseNetBodyPart leftEar;
    //public PoseNetBodyPart rightEar;
    //public PoseNetBodyPart leftShoulder;
    //public PoseNetBodyPart rightShoulder;
    //public PoseNetBodyPart leftElbow;
    //public PoseNetBodyPart rightElbow;
    //public PoseNetBodyPart leftWrist;
    //public PoseNetBodyPart rightWrist;
    //public PoseNetBodyPart leftHip;
    //public PoseNetBodyPart rightHip;
    //public PoseNetBodyPart leftKnee;
    //public PoseNetBodyPart rightKnee;
    //public PoseNetBodyPart leftAnkle;
    //public PoseNetBodyPart rightAnkle;
}

[Serializable]
public class PoseNetPoseSkeleton
{
    public PoseNetPose pose;
    public PoseNetKeyPoint[][] skeleton;
}

[Serializable]
public class PoseNetImage
{
    public int width;
    public int height;
}

[Serializable]
public class PoseNetData
{
    public PoseNetPoseSkeleton[] poses;
    public PoseNetImage image;
}

public class UnitySkeletonBone
{
    public Vector3 a;
    public Vector3 b;
}

public class UnitySkeleton
{
    public List<UnitySkeletonBone> bones;
}

public class UnityPose
{
    public List<Vector3> keypoints;
    public List<float> scores;
}

public class UnityPoseSkeleton
{
    public UnityPose pose;
    public UnitySkeleton skeleton;
}

public class MyMath
{
    // the function to map a value from one range to another
    // i have it from here:
    // https://forum.unity.com/threads/mapping-or-scaling-values-to-a-new-range.180090/
    public static float Mapped(float x, float in_min, float in_max, float out_min, float out_max){
        // clamp
        if((x >= in_max && in_max > in_min) || (x >= in_min && in_max < in_min)){
            return out_max;
        }else if((x <= in_min && in_max > in_min) || (x <= in_max && in_max < in_min)){
            return out_min;
        }

        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
}


/*************************************************************
 *          Receiver class
 */

public class PoseReceiver: MonoBehaviour {

    [Range(0.00001f, 0.99999f)]
    public float smoothing = 0.80f;

    public float zDistanceMin = 50.0f;
    public float zDistanceMax = 300.0f;

    [Range(0.0f, 1.0f)]
    public float minimumKeypointScore = 0.3f;

    public bool playRecordingInEditor = true;
    public TextAsset recording;

    public GameObject nextReceiver;

    List < Vector3 > poseNetPositions = new List < Vector3 > ();
    List < UnityPoseSkeleton > poseSkeletons = new List < UnityPoseSkeleton > ();

    #if UNITY_EDITOR
        private List < string > recordingLines;
        private float lastRecordingUpdate = 0;
        private float recordingUpdateInterval = 1.0f / 30.0f;
        private int recordingFrame = 0;

        void Start(){
            if (recording) {
                recordingLines = new List < string > (recording.text.Split(';'));
            }
        }

        void Update(){
            if(recording && playRecordingInEditor && Time.time > lastRecordingUpdate + recordingUpdateInterval){
                ReceivePoses(recordingLines[recordingFrame]);
                recordingFrame++;
                recordingFrame %= recordingLines.Count;
                lastRecordingUpdate = Time.time;
            }
        }
    #endif

    public void ReceivePoses(string posesString){
        if(posesString != null){
            if (nextReceiver) {
                nextReceiver.SendMessage("ReceivePoses", posesString, SendMessageOptions.DontRequireReceiver);
            }

            // sometimes there might be something else delivered than the poses data..
            // for that case we try first
            // https://docs.unity3d.com/ScriptReference/Debug.LogException.html
            try
            {
                // put the data in a PoseNetData object
                PoseNetData poseNetData = JsonUtility.FromJson < PoseNetData > (posesString);

                // log it
                //Debug.Log("length: " + poseNetData.poses.Length);

                // it is good practice to have only one single listener for websockets,
                // but we want to possible have more than one thing react
                // so we send messages inbetween Unity scripts from here
                // https://docs.unity3d.com/ScriptReference/Component.BroadcastMessage.html
                // https://docs.unity3d.com/ScriptReference/Component.SendMessage.html

                // send the raw posenet data
                BroadcastMessage("GetPoseNetPoses", poseNetData, SendMessageOptions.DontRequireReceiver);

                // let's check if there is a new user
                if(poseNetData.poses.Length > poseNetPositions.Count){
                    BroadcastMessage("GetPoseNetUserAdded", poseNetData.poses.Length, SendMessageOptions.DontRequireReceiver);
                    // we know someone is added, so we also have to add a new position
                    poseNetPositions.Add(new Vector3());
                }else if(poseNetData.poses.Length < poseNetPositions.Count){
                    BroadcastMessage("GetPoseNetUserRemoved", poseNetData.poses.Length, SendMessageOptions.DontRequireReceiver);
                    // we know someone left
                    // we don't know who left, so we just remove the last one
                    poseNetPositions.RemoveAt(poseNetPositions.Count - 1);
                }

                // make sure we don't shoot out of our data
                int maximumIndex = Math.Min(poseNetData.poses.Length, poseNetPositions.Count);

                // then go through all of them
                for(int i = 0; i < maximumIndex; i++){
                    PoseNetPose pose = poseNetData.poses[i].pose;

                    // out positions are related on the dimensions of the camera image.. wouldn't it be cool if they were not?
                    // this is how you can map them in the range of -1 to 1
                    // check out the Map function below if you want to see how it's done
                    // usually you can get a function like this if you just google "unity map value to range" or something like that
                    float newX = MyMath.Mapped(pose.keypoints[(int)SkeletonDefinitions.Nose].position.x, 0, poseNetData.image.width, -1, 1);
                    // for vertical position we do it the other way around
                    float newY = MyMath.Mapped(pose.keypoints[(int)SkeletonDefinitions.Nose].position.y, poseNetData.image.height, 0, -1, 1);
                    float earDistance = Mathf.Abs(pose.keypoints[(int)SkeletonDefinitions.RightEar].position.x - pose.keypoints[(int)SkeletonDefinitions.LeftEar].position.x);
                    float newZ = MyMath.Mapped(earDistance, zDistanceMin, zDistanceMax, 1, -1);

                    // Then we want to smooth the value
                    // so we make a newPosition, from which we can interpolate
                    Vector3 newPosition = new Vector3(newX, newY, newZ);

                    // Lerp is a function to interpolate between values
                    // https://docs.unity3d.com/ScriptReference/Vector3.Lerp.html
                    poseNetPositions[i] = Vector3.Lerp(newPosition, poseNetPositions[i], smoothing);
                }
                BroadcastMessage("GetPoseNetPositions", poseNetPositions, SendMessageOptions.DontRequireReceiver);

                // TODO: use just a single for loop
                for(int i = 0; i < poseNetData.poses.Length; i++){
                    Debug.Log("poseNetData " + i);
                    bool isNewPose = false;
                    if(poseSkeletons.Count == i){
                        UnityPoseSkeleton poseSkeleton = new UnityPoseSkeleton();
                        poseSkeleton.pose = new UnityPose();
                        poseSkeleton.pose.keypoints = new List<Vector3>();
                        poseSkeleton.pose.scores = new List<float>();
                        poseSkeleton.skeleton = new UnitySkeleton();
                        poseSkeletons.Add(poseSkeleton);
                        isNewPose = true;
                        Debug.Log("is new pose");
                    }

                    PoseNetPose pose = poseNetData.poses[i].pose;
                    UnityPose oldPose = poseSkeletons[i].pose;

                    for(int k = 0; k < pose.keypoints.Length; k++){
                        float newX = MyMath.Mapped(pose.keypoints[k].position.x, 0, poseNetData.image.width, -1, 1);
                        float newY = MyMath.Mapped(pose.keypoints[k].position.y, poseNetData.image.height, 0, -1, 1);
                        float earDistance = Mathf.Abs(pose.keypoints[(int)SkeletonDefinitions.RightEar].position.x - pose.keypoints[(int)SkeletonDefinitions.LeftEar].position.x);
                        float newZ = MyMath.Mapped(earDistance, zDistanceMin, zDistanceMax, 1, -1);
                        Vector3 newKeypoint = new Vector3(newX,newY,newZ);
                        if(isNewPose){
                            oldPose.keypoints.Add(newKeypoint);
                            oldPose.scores.Add(pose.keypoints[k].score);
                        } else {
                            if (pose.keypoints[k].score > minimumKeypointScore) {
                                oldPose.keypoints[k] = Vector3.Lerp(newKeypoint, oldPose.keypoints[k], smoothing);
                            }
                            oldPose.scores[k] = pose.keypoints[k].score;
                        }
                    }
                }

                while(poseSkeletons.Count > poseNetData.poses.Length){
                    poseSkeletons.RemoveAt(poseSkeletons.Count - 1);
                }
                BroadcastMessage("GetPoseNetPoseSkeletons", poseSkeletons, SendMessageOptions.DontRequireReceiver);
            }
            catch(Exception e){
                //Debug.LogException(e, this);
            }

        }
    }
}
