using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PoseRotate : MonoBehaviour
{

    public Vector3 rotationSpeed = new Vector3(1.0f,1.0f,1.0f);

    public void GetPoseNetPositions(List<Vector3> poseNetPositions)
    {
        // only change position if there is somebody
        if (poseNetPositions.Count > 0)
        {
            transform.Rotate(
                    poseNetPositions[0].x * rotationSpeed.x,
                    poseNetPositions[0].y * rotationSpeed.y,
                    poseNetPositions[0].z * rotationSpeed.z,
                    Space.Self);
        }
    }
}
