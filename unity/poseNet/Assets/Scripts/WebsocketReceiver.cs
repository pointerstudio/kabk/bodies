using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class WebsocketReceiver : MonoBehaviour {

    // public variables
    // https://docs.unity3d.com/Manual/VariablesAndTheInspector.html
    // the websocket server ip
    // if the server is on your own computer, just use "localhost"
    public string websocketServerIp = "localhost";
    // the websocket server port
    public string websocketServerPort = "9090";

    public bool stopReceiverPlayingRecording = true;

    public PoseReceiver poseReceiver;

    // Use this for initialization
    IEnumerator Start () {

        if (stopReceiverPlayingRecording) {
            poseReceiver.playRecordingInEditor = false;
            Debug.Log("disables playRecordingInEditor for poseReceiver");
        }

        // https://docs.unity3d.com/ScriptReference/String.html
        string websocketURI = string.Format("ws://{0}:{1}",websocketServerIp,websocketServerPort);
        WebSocket w = new WebSocket(new Uri(websocketURI));
		yield return StartCoroutine(w.Connect());

        Debug.Log("WebSocket connected to " + websocketURI);

        while (true)
		{
			string reply = w.RecvString();
            //Debug.Log(reply);
			if (reply != null)
			{

                // sometimes there might be something else delivered than the poses data..
                // for that case we try first
                // https://docs.unity3d.com/ScriptReference/Debug.LogException.html
                try
                {
                    //Debug.Log(reply);
                    poseReceiver.ReceivePoses(reply);
                }
                catch (Exception e)
                {
                    Debug.LogException(e, this);
                }

            }
			if (w.error != null)
			{
				Debug.LogError ("Error: "+w.error);
				break;
			}
			yield return 0;
		}
		w.Close();
	}

}
