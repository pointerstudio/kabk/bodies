/* KABK * IMD * skeleton tracking example
 *
 * webcam dots
 *
 * In this example, you'll see how to track skeletons from a webcam,
 * and draw dots where the joints would be.
 *
 */

// GLOBAL VARIABLES
// variables, that will be accessible anywhere on this website,
// even in other scripts (if they are loaded after this script)

// one for the videocamera
let video;

// one for poseNet, this is the skeleton (pose) detector
let poseNet;

// this is going to be our unity Instance
let myUnityInstance = false;

// one array for all poses
// an array is a list
let poses = [];

// the video size, smaller means faster
// first the width
// feel free to set this to 320 if it is otherwise tough for your computer
let videoWidth = 640;
// then the height
// feel free to set this to 240 if it is otherwise tough for your computer
let videoHeight = 480;

let TIME_TO_MOVE_ON = 5 * 60 * 1000;

function loadNextProject(projects) {
    setTimeout(() => {
        let nextProject = projects.pop();
        let projectsString = "";
        if (projects.length > 0) {
            projectsString = "?projects=" + JSON.stringify(projects);
        }
        let href = "https://kabk.pointer.click/" + getURLPath()[0] + "/" + nextProject + "/" + projectsString;
        location.href = href;
    }, TIME_TO_MOVE_ON);
}

function autoplay() {
    // first, give it a title
    // what about the last bit in our url in the top bar
    let urlPath = getURLPath();
    let title = urlPath[urlPath.length - 1]; // access second last element
    document.getElementById('title').innerHTML = '<p>' + title + '</p>';
    // set a timeout to remove the title after ten seconds
    setTimeout(function() {
        document.getElementById('title').remove();
    }, 10 * 1000); // ten seconds

    let urlParams = getURLParams();

    if (urlParams.hasOwnProperty('projects')) {
        let projectsString = decodeURI(urlParams.projects);
        console.log({urlParams,projectsString});
        let projects = JSON.parse(projectsString);
        projects = projects.filter(function(e) { return e.toLowerCase() !== title.toLowerCase(); });
        loadNextProject(projects);
    } else {
        let api_url = "../getProjects.php";
        httpGet(api_url, 'json', false, function(response) {
            let projects = response;
            projects = projects.filter(function(e) { return e.toLowerCase() !== title.toLowerCase(); });
            loadNextProject(projects);
        });
    }

}

// the setup function runs exactly once at the beginning
// we can use it to... set up things
function setup() {
    // first handle autoplay
    autoplay();

    // then, create a canvas to draw into
    // p5js uses a canvas for drawing
    createCanvas(windowWidth, windowHeight);

    // this is how p5js is using your webcam
    video = createCapture(VIDEO);
    // then give it the size that you want
    video.size(videoWidth, videoHeight);

    // Create a new poseNet method
    // call the modelReady function, when ready
    poseNet = ml5.poseNet(video, modelReady);

    // This sets up an event that fills the global variable "poses"
    // with an array every time new poses are detected
    poseNet.on('pose', function(results) {
        poses = results;
        if (myUnityInstance !== false) {
            let poseNetData = {
                "image": {
                    "width": videoWidth,
                    "height": videoHeight
                },
                "poses": poses

            };
            myUnityInstance.SendMessage('PoseNetObject', 'ReceivePoses', JSON.stringify(poseNetData));
        }
    });

    // Hide the video element, and just show the canvas
    video.hide();
}

// when posenet is ready it calls this function
function modelReady() {
    select('#status').html('Model Loaded');
}


function draw() {
    // first, let's draw a black background
    background(0);

    // then the video, just so that we see what it's doing
    image(video, 0, 0, videoWidth, videoHeight);

    // let's have different colors for different people (a.k.a. poses)
    let colors = ['red', 'green', 'blue', 'pink', 'purple'];

    // Loop through all the poses detected
    for (let i = 0; i < poses.length; i++) {
        // For each pose detected, loop through all the keypoints
        let pose = poses[i].pose;

        for (let j = 0; j < pose.keypoints.length; j++) {
            // A keypoint is an object describing a body part (like rightArm or leftShoulder)
            let keypoint = pose.keypoints[j];
            // Only draw an ellipse is the pose probability is bigger than 0.2
            if (keypoint.score > 0.2) {
                fill(colors[i]);
                noStroke();
                ellipse(keypoint.position.x, keypoint.position.y, 10, 10);
            }
        }
    }
}
