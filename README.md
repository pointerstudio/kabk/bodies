these are examples to use in the course IMD for KABK during the BODIES assignment

the examples are meant to be standalone. You will need to be online, when you first start them to download the machine learning model (don't worry, this will happen automatically). after that you should be able to work offline.

it is recommended not to change these examples themselves, but make a copy of them and change the copy. why? you could break something :) then it is great to have an example that just works.
also, these examples will be updated. it is then easier for you to update it.

# overview p5js examples
## 0_dots
this example shows you the power of poseNet. You should see the camera image and the joints of detected bodies.

## 1_basic
you don't need to use joints to make this useful for your work. here we use the most basic information: is somebody there or not.

## 2_multibasic
building up on the previous example, here we also detect presence, but can see how many bodies are there.

## 3_distance
quite intuitive can be to use the distance of a viewer. this example shows how to approximate this.

# overview unity examples
all examples reside in [one unity project](https://gitlab.com/pointerstudio/kabk/bodies/-/tree/main/unity/poseNet).
open a scene in _Assets/Scene_ to open another example

## BasicMoveScene
move a cube with your skeleton

## MoveScene
move more cubes with your skeleton

## RotateScene
rotate a horse with your skeleton

## TrackScene
no idea why I called it TrackScene, move around a cloud of text and a couple of rocks
